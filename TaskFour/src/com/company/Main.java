package com.company;

import java.util.concurrent.ThreadLocalRandom;
import java.util.Scanner;


public class Main {

    public static void main(String[] args) {
        boolean correctInput = true;
        System.out.print("Input matrix size: ");
        Scanner input = new Scanner(System.in);
        //1. Приложение считывает размерность матрицы a [n] [n]. Валидирует входные данные.
        int size = input.nextInt();
        if (size < 0){
            System.out.println("Incorrect input. Matrix size must be positive. Try again!");
            correctInput = false;
        }

        if (correctInput){
            int[][] board = new int[size][size];

            //2. Заполняет значения элементов матрицы в интервале значений от -M до M с помощью генератора случайных чисел (класс Random).
            for (int row = 0; row < board.length; row++) {
                for (int col = 0; col < board[row].length; col++) {
                    board[row][col] = ThreadLocalRandom.current().nextInt(-size, size);
                }
            }
            printMatrix(board);

            //3. Упорядочивает строки (столбцы) матрицы в порядке возрастания значений элементов k-го столбца (строки). Выводит полученную матрицу в консоль.
            //let k be random number
            int k = ThreadLocalRandom.current().nextInt(0,size-1);
            int[][] sortedMatrix = sortByColumn(board, k);
            printMatrix(sortedMatrix);

            //4. Находит сумму элементов матрицы, расположенных между первым и вторым положительными элементами каждой строки. Выводит номер строки, первый и второй положительный элемент и сумму элементов между ними в консоль.

            printElementSum(sortedMatrix);


            //5. Находит максимальный элемент в матрице и удалить из матрицы все строки и столбцы, его содержащие. Выводит полученную матрицу в консоль.
            int maxValue = findMax(sortedMatrix);
            System.out.println("Maximum value in the matrix is " + maxValue);

            int count = countMax(sortedMatrix, maxValue);

            for (int i = 0; i < count; i++) {
                sortedMatrix = deleteRowColumn(sortedMatrix, maxValue);
            }

            printMatrix(sortedMatrix);

        }
    }

    public static int[][] deleteRowColumn(int[][] matrix, int max) {
        int[] indexes = findIndexRowColMax(matrix, max);
        int deleteRow = indexes[0];
        int deleteCol = indexes[1];

        int[][] newMatrix = new int[matrix.length - 1][matrix[0].length - 1];
        int row = 0;

        for (int i = 0; i < newMatrix.length; i++) {
            if (i > deleteRow - 1) {
                row = 1;
            }
            int column = 0;

            for (int j = 0; j < newMatrix[0].length; j++) {
                if (j > deleteCol - 1) {
                    column = 1;
                }
                newMatrix[i][j] = matrix[i + row][j + column];
            }
        }
        return newMatrix;
    }

    public static int[] findIndexRowColMax(int[][] matrix, int max) {
        int[] indexes = new int[2];

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (max == matrix[i][j]) {
                    indexes[0] = i;
                    indexes[1] = j;
                }
            }
        }
        return indexes;
    }

    public static int countMax(int[][] matrix, int max) {
        int count = 0;

        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if (max == matrix[i][j]) {
                    count++;
                }
            }
        }
        return count;
    }

    private static int findMax(int[][] board) {
        int maxValue = board[0][0];

        for(int i = 0; i < board.length; i++) {
            for(int j = 0; j < board[i].length; j++) {
                if (board[i][j] > maxValue) {
                    maxValue = board[i][j];
                }
            }
        }
        return maxValue;
    }

    private static void printElementSum(int[][] board){
        for(int i = 0; i < board.length; i++) {
            int sumRow = 0;
            int startValue = board[i][0];
            int endValue = board[i][board.length-1];
            boolean finished = false;
            for(int j = 0; j < board[i].length; j++) {
                if (board[i][j] > 0) {
                    if (j > 0 && board[i][j-1] < 0){
                        startValue = board[i][j];
                    }
                    sumRow += board[i][j];
                } else if ((board[i][j] <= 0) && (sumRow != 0)) {
                    endValue = board[i][j - 1];
                    sumRow -= board[i][j-1];
                    finished = true;
                    break;
                }
            }

            if (finished) {
                System.out.println((i+1) +". Elements: " + startValue + ", " + endValue + "; Sum: " + sumRow + ";");
            } else {
                System.out.println((i+1) +". " +"There are not any positive number.");
            }
        }
    }

    private static int[][] sortByColumn(int[][] matrix, int k) {
        System.out.println("Sort by " + (k + 1) + " column: ");
        int[][] matrixSorted = matrix.clone();
        for (int i = 0; i < matrixSorted.length; i++) {
            for (int j = 0; j < matrixSorted.length - i - 1; j++) {
                if (matrixSorted[j][k] > matrixSorted[j + 1][k]) {
                    int[] tmp = matrixSorted[j];
                    matrixSorted[j] = matrixSorted[j + 1];
                    matrixSorted[j + 1] = tmp;
                }
            }
        }
        return matrixSorted;
    }

    private static void printMatrix(int[][] board) {
        for(int i = 0; i < board.length; i++) {
            for(int j = 0; j < board[i].length; j++) {
                System.out.print(board[i][j] + " ");
            }
            System.out.println();
        }
    }

}
